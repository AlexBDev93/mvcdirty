<?php

/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 06/04/17
 * Time: 14:53
 */
class AppKernel
{
    protected $bundles = [];
    private $rootDir;
    private $container;

    public function registerBundles()
    {
        $bundles = [];
        $bundles[] = new \Framework\Template\Configuration();

        $this->bundles = $bundles;
    }

    public function getBundles()
    {
        return $this->bundles;
    }

    public function getRootDir()
    {
        if (null === $this->rootDir) {
            $r = new \ReflectionObject($this);
            $this->rootDir = dirname($r->getFileName());
        }

        return $this->rootDir;
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    private function setupContainer()
    {
        $this->registerBundles();
        $this->container = new \Framework\Kernel\Container();
        $configuration = \Symfony\Component\Yaml\Yaml::parse(file_get_contents($this->getRootDir().'/config/config.yml'));
        $this->container->set('kernel.root_dir', $this->getRootDir());

        $configurator = new \Framework\Kernel\Configurator($configuration);
        foreach ($this->bundles as $bundle) {
            $services = $configurator->createServices($bundle);
            foreach ($services as $id => $service) {
                $this->container->set($id, $service);
            }
        }
    }

    public function handle()
    {
        $this->setupContainer();
    }
}