<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 03/04/17
 * Time: 09:49
 */

namespace App\Controller;


use \Framework\Controller\BaseController;

class FrontController extends BaseController
{
    /**
     * @Route(path="/", name="homepage")
     */
    public function indexAction()
    {
        $template = $this->get('dildo_template');
        $content = $template->render('layout.tpl', [
            'content' => $template->render('home.tpl')
        ]);

        echo $content;
    }

    /**
     * @Route(path="/tell/{something}", name="tell")
     */
    public function tellAction($something)
    {
        $template = $this->get('dildo_template');
        $content = $template->render('layout.tpl', [
            'content' => $template->render('tell.tpl', ['name' => $something])
        ]);

        echo $content;
    }
}