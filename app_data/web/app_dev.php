<?php

require '../framework/Psr4AutoloaderClass.php';
require '../vendor/autoload.php';
require '../AppKernel.php';

/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 03/04/17
 * Time: 09:37
 */

$loader = new Psr4AutoloaderClass();

// register the autoloader
$loader->register();

// register the base directories for the namespace prefix
$loader->addNamespace('App', '../src/App/');
$loader->addNamespace('Framework', '../framework/Framework');

$annotation = new \Framework\Annotation\Reader();
$annotations = $annotation->getClassMethodsAnnotations(\App\Controller\FrontController::class);
$request = new \Framework\Request\Request($_REQUEST, $_SERVER);
$routes = new \Framework\Router\RouteResolver();

foreach ($annotations as $annotation) {
    if ($annotation->getName() === 'Route') {
        $route = new \Framework\Router\Route();
        $route
            ->setClass($annotation->getClass())
            ->setMethod($annotation->getMethod())
            ->setName($annotation->getAttributes()['name'])
            ->setPath($annotation->getAttributes()['path'])
        ;
        $routes->addRoute($route);
    }
}

$router = new \Framework\Router\Router($request, $routes);

$kernel = new AppKernel();
$kernel->handle($request);

$router->executeControllerAction($kernel->getContainer());