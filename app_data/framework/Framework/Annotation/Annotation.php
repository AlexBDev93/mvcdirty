<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 03/04/17
 * Time: 10:30
 */

namespace Framework\Annotation;


class Annotation
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Annotation
     */
    public function setName(string $name): Annotation
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return Annotation
     */
    public function setClass(string $class): Annotation
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return Annotation
     */
    public function setMethod(string $method): Annotation
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     * @return Annotation
     */
    public function setAttributes(array $attributes): Annotation
    {
        $this->attributes = $attributes;
        return $this;
    }
}