<?php

namespace Framework\Annotation;

/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 03/04/17
 * Time: 10:00
 */
class Reader
{
    protected $annotations = [];

    /**
     * @return array
     */
    public function getAnnotations(): array
    {
        return $this->annotations;
    }

    /**
     * @param array $annotations
     * @return Reader
     */
    protected function setAnnotations(array $annotations): Reader
    {
        $this->annotations = $annotations;

        return $this;
    }

    /**
     * @param string $class
     * @return array
     */
    public function getClassMethodsAnnotations(string $class)
    {
        $reflection = new \ReflectionClass($class);
        $methods = $reflection->getMethods();
        $annotations = [];
        foreach ($methods as $method) {
            $methodName = $method->getName();
            $doc = $reflection->getMethod($methodName)->getDocComment();
            preg_match_all('#@(.*?)\n#s', $doc, $match);

            foreach ($match[1] as $rawAnnotation) {
                $annotation = new Annotation();
                $annotation
                    ->setClass($class)
                    ->setMethod($methodName);

                $attributes = Parser::parse($rawAnnotation);
                $annotation
                    ->setName($attributes[0])
                    ->setAttributes($attributes[1]);
            }

            $annotations[] = $annotation;
        }
        $this->setAnnotations($annotations);

        return $this->getAnnotations();
    }
}