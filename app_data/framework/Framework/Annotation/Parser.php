<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 03/04/17
 * Time: 10:21
 */

namespace Framework\Annotation;


class Parser
{
    public $annotations;

    /**
     * @return mixed
     */
    public function getAnnotations()
    {
        return $this->annotations;
    }

    /**
     * @param mixed $annotations
     * @return Parser
     */
    public function setAnnotations($annotations)
    {
        $this->annotations = $annotations;
        return $this;
    }

    public static function parse($annotation)
    {
        $parse = [];
        $exploded = explode('(', $annotation, 2);
        if (isset($exploded[0])) {
            $parse[0] = $exploded[0];
        }
        $parse[1] = [];

        if (isset($exploded[1])) {
            $rawParameters = rtrim($exploded[1], ')');
            $rawParameters = explode(',', $rawParameters);
            foreach ($rawParameters as $rawParameter) {
                $tmp = explode('=', $rawParameter);
                $key = $tmp[0];
                $hasValue = true;
                $value = null;
                if (isset($tmp[1])) {
                    $value = $tmp[1];
                } else {
                    $value = $tmp[0];
                    $hasValue = false;
                }
                $value = str_replace('"', '', $value);
                $value = str_replace('\'', '', $value);
                $value = trim($value);
                $key = trim($key);

                $resolved = [];
                if (!$hasValue) {
                    $resolved = $value;
                } else if ($value !== null) {
                    $resolved[$key] = $value;
                }

                $parse[1] = array_merge($parse[1], $resolved);
            }
        }

        return $parse;
    }
}