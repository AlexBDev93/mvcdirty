<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 06/04/17
 * Time: 11:44
 */

namespace Framework\Template;


class Configuration implements \Framework\Kernel\Configuration
{
    /**
     * @return string
     */
    public static function getId(): string
    {
        return 'framework_orm';
    }

    /**
     * @return array
     */
    public static function defineConfiguration(): array
    {
        return [
            'user',
            'password',
            'database',
        ];
    }

    /**
     * @param array $configuration
     * @return array
     * @throws \Exception
     */
    public static function resolveConfiguration(array $configuration): array
    {
        $defined = self::defineConfiguration();
        $resolved = [];
        foreach ($defined as $name => $arg) {
            if (!array_key_exists($name, $configuration)) {
                throw new \Exception('The parameter '.$name.' must be define under '.self::getId());
            }

            $value = null;
            if (is_callable($arg)) {
                $value = call_user_func($arg, $configuration[$name]);
            } else {
                $value = $configuration[$name];
            }

            $resolved[$name] = $value;
        }

        return $resolved;
    }
}