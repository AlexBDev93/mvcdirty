<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 06/04/17
 * Time: 11:59
 */

namespace Framework\Kernel;


interface Configuration
{
    public static function getId(): string;

    public static function defineConfiguration(): array;
}