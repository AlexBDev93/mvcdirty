<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 06/04/17
 * Time: 11:49
 */

namespace Framework\Kernel;


use Symfony\Component\Yaml\Yaml;

class Configurator
{
    protected $fqcn;

    protected $configuration;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @return mixed
     */
    protected function getFqcn()
    {
        return $this->fqcn;
    }

    /**
     * @param mixed $fqcn
     * @return Configurator
     */
    protected function setFqcn($fqcn)
    {
        $this->fqcn = $fqcn;
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    private function resolvedConfig()
    {
        $fqcn = $this->getFqcn();
        if (!class_implements($fqcn, Configuration::class)) {
            throw new \Exception('The class '.$fqcn.' must implement '.Configuration::class);
        }

        $this->configuration[$fqcn::getId()] = ConfigurationResolver::resolveConfiguration(
            $fqcn::getId(),
            $fqcn::defineConfiguration(),
            $this->getClassConfiguration()
        );

        return $this;
    }

    /**
     * @return string
     */
    private function getRessourcesDir()
    {
        $reflector = new \ReflectionClass($this->getFqcn());

        return dirname($reflector->getFileName());
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function getClassConfiguration()
    {
        $class = $this->getFqcn();
        $id = $class::getId();
        $configuration = $this->getConfiguration();

        if (!array_key_exists($id, $configuration)) {
            throw new \Exception('Unable to find configuration for '.$class);
        }

        return $configuration[$id];
    }

    /**
     * @param Configuration $configObject
     * @return array
     * @throws \Exception
     */
    public function createServices(Configuration $configObject)
    {
        $this->setFqcn(get_class($configObject));
        $this->resolvedConfig();
        $configuration = $this->getClassConfiguration();
        $file = $this->getRessourcesDir().'/config/services.yml';
        $services = [];
        if (file_exists($file)) {
            $content = Yaml::parse(file_get_contents($file));
            foreach ($content['services'] as $id => $service) {
                $class = $service['class'];
                $arguments = [];
                if (isset($service['arguments'])) {
                    foreach ($service['arguments'] as $arg) {
                        if (preg_match('/^%.*%$/', $arg) === 1) {
                            $argKey = str_replace('%', '', $arg);
                            if (!array_key_exists($argKey, $configuration)) {
                                throw new \Exception('Unable to found parameter '. $argKey);
                            }

                            $arg = $configuration[$argKey];
                        }
                        $arguments[] = $arg;
                    }
                }
                $services[$id] = new $class(...$arguments);
            }
        }

        return $services;
    }
}