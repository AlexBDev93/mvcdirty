<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 06/04/17
 * Time: 16:04
 */

namespace Framework\Kernel;


class ConfigurationResolver
{
    public static function resolveConfiguration(string $id, array $defined, array $configuration): array
    {
        $resolved = [];
        foreach ($defined as $name => $arg) {
            if (!array_key_exists($name, $configuration)) {
                throw new \Exception('The parameter '.$name.' must be define under '.$id);
            }

            $value = null;
            if (is_callable($arg)) {
                $value = call_user_func($arg, $configuration[$name]);
            } else {
                $value = $configuration[$name];
            }

            $resolved[$name] = $value;
        }

        return $resolved;
    }
}