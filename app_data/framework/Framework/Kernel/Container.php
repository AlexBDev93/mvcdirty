<?php

namespace Framework\Kernel;

/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 06/04/17
 * Time: 10:34
 */
class Container
{
    private $_services = [];

    final public function get($id)
    {
        if (array_key_exists($id, $this->_services)) {
            return $this->_services[$id];
        }

        throw new \Exception('The service '.$id.' has been not found.');
    }

    final public function has(string $id)
    {
        return array_key_exists($id, $this->_services);
    }

    public function set(string $id, $service)
    {
        $this->_services[$id] = $service;

        return $this;
    }
}