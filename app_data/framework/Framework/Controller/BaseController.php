<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 03/04/17
 * Time: 09:51
 */

namespace Framework\Controller;


use Framework\Kernel\Container;

class BaseController
{
    /**
     * @var Container
     */
    private $_container;

    /**
     * @return Container
     */
    final public function getContainer()
    {
        return $this->_container;
    }

    /**
     * @param Container $container
     * @return $this
     */
    final public function setContainer(Container $container)
    {
        $this->_container = $container;
        return $this;
    }

    final public function get($id)
    {
        return $this->_container->get($id);
    }

    final public function has($id)
    {
        return $this->_container->has($id);
    }
}