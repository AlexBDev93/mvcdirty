<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/04/17
 * Time: 10:45
 */

namespace Framework\Template;

class Template {

    /**
     * @var string
     */
    protected $baseDir = '';

    public function __construct(string $baseDir)
    {
        $this->baseDir = $baseDir;
    }

    /**
     * @return string
     */
    public function getBaseDir()
    {
        return $this->baseDir;
    }

    /**
     * @param mixed $baseDir
     * @return Template
     */
    public function setBaseDir(string $baseDir): Template
    {
        $this->baseDir = $baseDir;
        return $this;
    }

    /**
     * @param $file
     * @param $parameters
     * @return string
     */
    public function render(string $file, array $parameters = [])
    {
        $filepath = $this->getFilepath($file);
        if (!file_exists($filepath)) {
            return 'Error loading template file ('.$filepath.').<br />';
        }

        $output = file_get_contents($filepath);

        foreach ($parameters as $key => $value) {
            $tagToReplace = "[@$key]";
            $output = str_replace($tagToReplace, $value, $output);
        }

        return $output;
    }

    /**
     * @param $file
     * @return string
     */
    private function getFilepath($file)
    {
        return rtrim($this->baseDir, '/').'/'.trim($file, '/');
    }
}
