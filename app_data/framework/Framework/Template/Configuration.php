<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 06/04/17
 * Time: 11:44
 */

namespace Framework\Template;


use Framework\Kernel\BaseConfiguration;

class Configuration implements \Framework\Kernel\Configuration
{
    /**
     * @return string
     */
    public static function getId(): string
    {
        return 'framework_template';
    }

    /**
     * @return array
     */
    public static function defineConfiguration(): array
    {
        return [
            'base_dir' => function ($path) {
                $absolutePath = realpath($path);
                if ($absolutePath === false) {
                    throw new \Exception('Unable to resolve the direcotry path "'.$path.'"');
                }

                return $absolutePath;
            }
        ];
    }
}