<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 03/04/17
 * Time: 13:17
 */



namespace Framework\Router;


use Framework\Request\Request;

class Router
{
    protected $_routes = [];

    protected $_request;

    /**
     * Router constructor.
     * @param Request $request
     */
    public function __construct(Request $request, RouteResolver $route)
    {
        $this->_request = $request;
        $this->_routes = $route;
    }
    
    public function executeControllerAction($container)
    {
        $requestPath = $this->_request->getUri();
        $route = $this->_routes->getRouteByPath($requestPath);
        $routePath = $route->getPath();

        if ($routePath === null) {
            throw new \Exception('Any route found for path '.$requestPath);
        }


        $requestAttr = explode('/', trim($requestPath, '/'));
        $routeAttr = explode('/', trim($routePath, '/'));
        $parameters = [];
        foreach ($routeAttr as $key => $value) {
            if (!isset($requestAttr[$key])) {
                throw new \Exception('Request parameter doesn\'t match route parameters');
            }

            if ($value !== $requestAttr[$key]) {
                $parameters[] = $requestAttr[$key];
            }
        }

        $class = $route->getClass();
        $controller = new $class();
        $controller->setContainer($container);
        $method = $route->getMethod();

        call_user_func_array([$controller, $method], $parameters);
    }
}