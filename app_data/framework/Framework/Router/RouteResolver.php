<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 03/04/17
 * Time: 13:27
 */

namespace Framework\Router;


class RouteResolver
{
    protected $routes = [];

    public function addRoute(Route $route)
    {
        $this->routes[] = $route;
    }

    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param string $class
     * @param string $method
     * @return Route|null
     */
    public function getRouteByControllerAction(string $class, string $method)
    {
        foreach ($this->routes as $route) {
            if ($route->getClass() === $class && $route->getMethod() === $method) {
                return $route;
            }
        }

        return null;
    }

    /**
     * @param string $requestPath
     * @return mixed|null
     */
    public function getRouteByPath(string $requestPath)
    {
        foreach ($this->routes as $route) {
            $originalPath = $route->getPath();
            $path = preg_replace('({.*})', '[\w]+', $originalPath);
            $pattern = '/^'.str_replace('/', '\/', $path).'\/?$/';
            if (preg_match($pattern, $requestPath) === 1) {
                return $route;
            }
        }

        return null;
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getRouteByName(string $name)
    {
        foreach ($this->routes as $route) {
            if ($route->getName() === $name) {
                return $route;
            }
        }

        return null;
    }
}
